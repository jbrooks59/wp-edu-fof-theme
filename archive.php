<?php
/* 
Template Name: Archives
*/
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wp_edu_theme
 */

get_header();
?>
<div class="content__border">
    <div class="contentMain row">
		<div class="sideBar col col--3">
			<?php get_sidebar(); ?>
		</div>
		<div class="contentWithSidebar col col--9">
			<div class="mobileSidebar">
				<div class="accordion accordion--mobile" role="presentation">
					<div class="accordion_item">
						<div class="accordion_title" role="heading" tabindex="0" aria-expanded="false" aria-controls="content-0" id="accordion-0">
							<p>Additional Navigation</p>
						</div>
						<div class="accordion_content" role="region" aria-hidden="true" aria-labelledby="accordion-0" id="content-0">
							<?php include('/app/public/wp-content/themes/wp-edu-theme/sidebar.php'); ?>
						</div>
					</div>
				</div>
			</div>

			<?php query_posts(array('post_type' => 'post','orderby' => 'date')); 
				if(have_posts()) : while(have_posts()) : the_post(); ?>

					<div class="row archive__row">
						<div class="archive__left col col--8">
							<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
							<?php the_excerpt(); ?>
							<?php ; ?>
							<div class="archive__bottomText">
								<span class="archive__timestamp"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>|
								<a class="archive__readMore" href="<?php the_permalink() ?>">Read More</a>
							</div>
						</div>
						<div class="archive__right col col--4">
							<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'full', array( 'class' => 'archive__featureImg' ) ); ?></a>
						</div>
					</div>
				<?php endwhile; ?>
				<?php else : ?>

					<p>sorry no results</p>

				<?php endif; wp_reset_query(); ?>

	</div>
</div>
<?php
get_footer();
