<?php
/**
 * Template Name: FOF Content Page
 * Template Post Type: page
 */
get_header();
?>
<link href="/wp-content/themes/wp-edu-fof-theme/style.css" rel="stylesheet">
<div class="content__border">
    
    <div class="contentMain contentMain--full" data-swiftype-name="body" data-swiftype-type="text">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <main id="main" class="site-main">
                <?php
                    // Title
                    the_title( '<h1 class="entry-title">', '</h1>' );
                    //Post Thumbnail
                    if(has_post_thumbnail()) {
                        echo "<div class='featuredImage__block'>";
                        the_post_thumbnail( 'full', array( 'class' => 'featuredImage' ) ); 
                        if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) {
                            echo "<p class='featuredImage__caption'>".$caption."</p>";
                        }
                        echo "</div>";
                    }  
                    if ( have_posts() ) : while ( have_posts() ) : the_post();
                        the_content(); // displays whatever you wrote in the wordpress editor
                    endwhile; endif; //ends the loop
                ?>
            </main><!-- #main -->
        </article>
        <?php
            function new_excerpt_more( $more ) {
                return '...';
            }
            add_filter( 'excerpt_more', 'new_excerpt_more' );
			$orig_post = $post;
			global $post;
			$categories = get_field('category');
			if ($categories) {
                $counter = 0;
				$category_ids = array();
				$category_ids[] = $categories;
				
				$args=array(
				'category__in' => $category_ids,
				'post__not_in' => array($post->ID),
				//'posts_per_page'=> 4, // Number of related posts that will be shown.
				'caller_get_posts'=>1
				);
				
				$my_query = new wp_query( $args );
				if( $my_query->have_posts() ) {
					echo '<div id="relatedPosts">
							<h3 class="relatedPosts__title relatedPosts__title--main">Begin the 9-Part Foundations of Faith Series</h3>
							<div class="row">';
					while( $my_query->have_posts() ) {
                        $counter++;
						$my_query->the_post();
						echo '<div class="excerptColumn col col--4">';
                        echo the_post_thumbnail( "full", array( "class" => "relatedPosts__thumb excerptImage" ) );
                        echo the_title('<h3 class="relatedPosts__title">', '</h3>');
                        echo "<p class=\"excerptParagraph\">" . get_the_excerpt() . "</p>";
                        echo '<a href="' . get_permalink() .'" class=" excerptButton btn btn--large">' . get_field('post_buttons') . '</a>';
                        echo '</div>';
                        
					}
				echo '</div></div>';
				}
			}
			$post = $orig_post;
			wp_reset_query(); ?>
    </div>
</div>
<?php get_footer(); ?>